export class Heroslides {
    private runner: NodeJS.Timer;

    public constructor() {
        const ces = document.querySelectorAll('[data-ctype=ce_heroslides]') as NodeListOf<HTMLDivElement>;

        setTimeout(() => {
            ces.forEach((ce: HTMLDivElement) => {
                const items = ce.querySelectorAll('.contents .item') as NodeListOf<HTMLDivElement>;

                items.forEach(item => {
                    item.classList.contains('active') && (item => {
                        item.classList.add('fade-in');

                        this.runner = setInterval(() => {
                            this.interval(ce, items);
                        }, 5000);
                    })(item);
                });

                const navItems = ce.querySelectorAll('.navigation .inner .item') as NodeListOf<HTMLDivElement>;

                navItems.forEach(navItem => {
                    navItem.onclick = e => {
                        const target = e.currentTarget as HTMLDivElement;
                        const uid = parseInt(target.getAttribute('data-uid') as string);

                        if (target.classList.contains('active')) {
                            return;
                        }

                        (ce.querySelector('.navigation .inner .item.active') as HTMLDivElement).classList.remove('active');
                        target.classList.add('active');

                        const activeItem = ce.querySelector('.contents .item.active') as HTMLDivElement;
                        const item = ce.querySelector(`.contents .item[data-uid='${uid}']`) as HTMLDivElement;

                        activeItem.classList.remove('active');
                        activeItem.classList.remove('fade-in');

                        item.classList.add('active');
                        item.classList.add('fade-in');
                        ce.classList.add('user-selected');

                        clearInterval(this.runner);
                    };
                });
            });
        }, 0);
    }

    private interval(ce: HTMLDivElement, items: NodeListOf<HTMLDivElement>) {
        // updating UI content
        const active = ce.querySelector('.item.active') as HTMLDivElement;
        const uid = parseInt(active.getAttribute('data-uid') as string);

        active.classList.add('fade-out');

        setTimeout(() => {
            active.classList.remove('active');

            let nextActive = active.nextElementSibling as HTMLDivElement;

            if (!nextActive) {
                nextActive = items[0] as HTMLDivElement;
            }

            nextActive.classList.add('active');
            nextActive.classList.add('fade-in');

            // updating UI navigation
            const nextUid = parseInt(nextActive.getAttribute('data-uid') as string);
            const activeNaviItem = ce.querySelector(`.navigation .inner .item[data-uid='${uid}']`) as HTMLDivElement;
            let nextNaviItem = ce.querySelector(`.navigation .inner .item[data-uid='${nextUid}']`) as HTMLDivElement;

            if (!nextNaviItem) {
                nextNaviItem = ce.querySelectorAll('.navigation .inner .item')[0] as HTMLDivElement;
            }

            activeNaviItem.classList.remove('active');
            nextNaviItem.classList.add('active');

            // removing old active state
            active.classList.remove('fade-out');
            active.classList.remove('fade-in');
        }, 333);
    }
}
