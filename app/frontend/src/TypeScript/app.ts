import '../Scss/app.scss';
import { Accordions } from './Contents/Accordions';
import { Container } from './Core/Container';
import Waves from 'node-waves';
import { Heroslides } from './Contents/Heroslides';

const meta: ImportMeta = import.meta;

if (meta.hot) {
    meta.hot.accept();
}

interface BaseApplication {
    getContainer: Function;
}

class Application implements BaseApplication {
    private container: Container;

    public constructor() {
        Waves.init();

        this.container = new Container();

        new Accordions();
        new Heroslides();
    }

    getContainer(): Container {
        return this.container;
    }
}

new Application();

console.log('test222');
