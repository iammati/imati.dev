import { defineConfig, UserConfig, ViteDevServer } from 'vite'
import { resolve } from 'path'
import eslintPlugin from 'vite-plugin-eslint'
import timeReporter from 'vite-plugin-time-reporter'
import FullReload from 'vite-plugin-full-reload'
import { exec } from './helper/execute'

const production = process.env.NODE_ENV === 'production'

const configuration: UserConfig = {
    base: '/dist/',

    plugins: [
        eslintPlugin(),
        timeReporter(),

        {
            name: 'typo3-flush-caches',
            enforce: 'pre',

            handleHotUpdate() {
                // exec('typo3cms cache:flushtags lowlevel');
                // exec('typo3cms cache:flushtags pages');
                // exec('typo3cms cache:flushtags all');
                // exec('rm -r ../../var/cache');
            }
        },

        FullReload([
            resolve('../extensions/**/*.html'),
            resolve('../extensions/**/*.php'),
            resolve('../extensions/**/*.typoscript')
        ])
    ],

    build: {
        minify: production,
        outDir: resolve(__dirname, '../../public/dist'),
        target: 'esnext',
        emptyOutDir: true,
        manifest: true,
        sourcemap: true,
        watch: {
            include: './src/**'
        },
        rollupOptions: {
            input: ['./src/TypeScript/app.ts'],
        },
    },

    // HMR server-port which is exposed by DDEV-Local in .ddev/docker-compose.hmr.yaml
    server: {
        port: 3000,

        // WSL2 support
        watch: {
            usePolling: true,
        }
    }
}

export default defineConfig(configuration)
