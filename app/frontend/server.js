const { resolve } = require('path');
const { createServer } = require('vite')

; (async () => {
    const server = await createServer({
        configFile: 'frontend/vite.config.ts',
        root: __dirname+'/..',
        server: {
            port: 3001
        }
    })

    server.watcher.add(resolve('**/*.html'));
    server.watcher.add(resolve('**/*.php'));
    server.watcher.add(resolve('**/*.typoscript'));

    await server.listen()
})()
