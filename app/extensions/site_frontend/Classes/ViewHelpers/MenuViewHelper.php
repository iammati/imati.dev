<?php

declare(strict_types=1);

namespace Site\Frontend\ViewHelpers;

use B13\Menus\Domain\Repository\MenuRepository;
use Site\Frontend\Service\PageService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class MenuViewHelper extends AbstractViewHelper
{
    protected MenuRepository $menuRepository;
    protected PageService $pageService;

    public function __construct()
    {
        $this->menuRepository = GeneralUtility::makeInstance(MenuRepository::class);
        $this->pageService = GeneralUtility::makeInstance(PageService::class);
    }

    public function initializeArguments()
    {
        $this->registerArgument(
            'pageUid',
            'integer',
            '',
            true
        );

        $this->registerArgument(
            'depth',
            'integer',
            '',
            true
        );

        $this->registerArgument(
            'as',
            'string',
            ''
        );
    }

    public function render()
    {
        $pageUid = (int)$this->arguments['pageUid'];
        $depth = (int)$this->arguments['depth'];
        $as = $this->arguments['as'];

        $tree = $this->menuRepository->getPageTree($pageUid, $depth, []);

        // $currentPage = $this->pageService->getPage();
        // $this->templateVariableContainer->add('currentPage', $currentPage);

        if ($as === null) {
            return $tree;
        }

        $this->templateVariableContainer->add($as, $tree);
    }
}
