<?php

declare(strict_types=1);

namespace Site\Frontend\ViewHelpers\Ce;

/*
 * This file is part of the FluidTYPO3/Vhs project under GPLv2 or later.
 *
 * For the full copyright and license information, please read the
 * LICENSE.md file that was distributed with this source code.
 */

use Site\Frontend\Traits\TemplateVariableViewHelperTrait;

/**
 * ViewHelper used to render content elements in Fluid templates.
 *
 * ### Render a single content element by its UID
 *
 * Let's assume that the variable `settings.element.uid` contains the uid
 * of a content element.
 * It can be rendered as follows:
 *
 *     <site:ce.render contentUids="{0: settings.element.uid}" />
 */
class RenderViewHelper extends AbstractContentViewHelper
{
    use TemplateVariableViewHelperTrait;

    public function initializeArguments(): void
    {
        parent::initializeArguments();
        $this->registerAsArgument();
    }

    public function render(): string
    {
        if (TYPO3_MODE === 'BE') {
            return '';
        }

        $content = $this->getContentRecords();

        if ($this->hasArgument('as') === false) {
            return implode(LF, $content);
        }

        return $this->renderChildrenWithVariableOrReturnInput($content);
    }
}
