<?php

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Extbase\Utility\ExtensionUtility;

(function () {
    ExtensionUtility::configurePlugin(
        'SiteStorys',
        'Pi1',

        [\Site\SiteStorys\Controller\StoryController::class => 'list, show'],
        [\Site\SiteStorys\Controller\StoryController::class => 'list, show'],
    );

    ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    pi1 {
                        title = LLL:EXT:site_storys/Resources/Private/Language/locallang_db.xlf:tx_sitestorys_pi1.name
                        description = LLL:EXT:site_storys/Resources/Private/Language/locallang_db.xlf:tx_sitestorys_pi1.description

                        tt_content_defValues {
                            CType = list
                            list_type = sitestorys_pi1
                        }
                    }
                }

                show = *
            }
        }'
    );
})();
