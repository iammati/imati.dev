<?php

declare(strict_types=1);

namespace Site\SiteStorys\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

class Story extends AbstractEntity
{
    protected ?\TYPO3\CMS\Extbase\Domain\Model\FileReference $image = null;
    protected string $header;
    protected string $subheader;
    protected string $description;
    protected string $link;
    protected string $linktext;

    public function setImage($image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setHeader($header): self
    {
        $this->header = $header;

        return $this;
    }

    public function getHeader()
    {
        return $this->header;
    }

    public function setSubheader($subheader): self
    {
        $this->subheader = $subheader;

        return $this;
    }

    public function getSubheader()
    {
        return $this->subheader;
    }

    public function setDescription($description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setLink($link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getLink()
    {
        return $this->link;
    }

    public function setLinktext($linktext): self
    {
        $this->linktext = $linktext;

        return $this;
    }

    public function getLinktext()
    {
        return $this->linktext;
    }
}
