<?php

declare(strict_types=1);

namespace Site\SiteStorys\Controller;

use Psr\Http\Message\ResponseInterface;
use Site\SiteStorys\Domain\Model\Story;
use Site\SiteStorys\Domain\Repository\StoryRepository;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

class StoryController extends ActionController
{
    protected StoryRepository $storyRepository;

    public function __construct(StoryRepository $storyRepository)
    {
        $this->storyRepository = $storyRepository;
    }

    public function listAction(): ResponseInterface
    {
        return $this->htmlResponse();
    }

    public function showAction(Story $story): ResponseInterface
    {
        edd($story);

        return $this->htmlResponse();
    }
}
