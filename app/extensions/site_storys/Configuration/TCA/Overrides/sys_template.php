<?php

defined('TYPO3_MODE') || die('Access denied.');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'SiteStorys',
    'Configuration/TypoScript',
    'Site - Storys'
);
