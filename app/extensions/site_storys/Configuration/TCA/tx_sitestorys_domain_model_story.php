<?php

use Site\Core\Form\Fields;

return Fields\Inline::make('Story', [
    'label' => 'header',

    'columns' => [
        'image' => Fields\Image::make('Image', [
            'fieldName' => 'image'
        ]),

        'subheader' => Fields\Input::make('Subheader'),
        'header' => Fields\Input::make('Header'),
        'description' => Fields\RTE::make('Description'),
    ],
]);
