<?php

declare(strict_types=1);

namespace Site\Dev\Service;

use Exception;
use TYPO3\CMS\Core\SingletonInterface;

class ViteService implements SingletonInterface
{
    protected string $publicPath = '/var/www/html/public';
    protected string $indexEntry = 'src/TypeScript/app.ts';

    /** @throws Exception */
    public function render(string $resolve)
    {
        if (!in_array($resolve, ['head', 'body'])) {
            throw new Exception(
                sprintf(
                    'Can not resolve "%s". Available values are: "head" | "body".',
                    $resolve
                ),
                1635708161
            );
        }

        $publicPath = $this->publicPath;
        $indexEntry = $this->indexEntry;
        $distPath = "${publicPath}/dist";

        $devServerIsRunning = $this->isDevServerRunning($distPath);

        if ($devServerIsRunning) {
            $devServerUri = $this->getDevServerUri();

            if ($resolve === 'head') {
                return <<<HTML
                    <script type="module" src="{$devServerUri}/dist/@vite/client"></script>
                HTML;
            } elseif ($resolve === 'body') {
                return <<<HTML
                    <script type="module" src="{$devServerUri}/dist/{$indexEntry}"></script>
                HTML;
            }

            return;
        }

        $manifest = json_decode(file_get_contents(
            "${distPath}/manifest.json"
        ), true);

        $entry = $manifest[$indexEntry];

        if ($resolve === 'head' && isset($entry['css'])) {
            return <<<HTML
                <link rel="stylesheet" href="/dist/{$entry['css'][0]}">
            HTML;
        }

        if ($resolve === 'body') {
            return <<<HTML
                <script type="module" src="/dist/{$entry['file']}"></script>
            HTML;
        }
    }

    /**
     * Retrieving via dot-env (.env) file the protocol-scheme
     * and base-domain to know where the vite-client js is deposited.
     *
     * The env-value PROTOCOL_SCHEME represents (probably due to DDEV-Local)
     * "https" while the BASE_DOMAIN can be anything e.g. project-name.ddev.site
     * the server-port will be appended for vitejs/vite + HMR.
     */
    private function getDevServerUri(): string
    {
        return env('PROTOCOL_SCHEME').'://'.env('BASE_DOMAIN').':'.(int)env('HMR_PORT');
    }

    /**
     * Handling if the current (frontend-)request was made while
     * the dev-server (vitejs/vite watcher) is running or not.
     */
    private function isDevServerRunning(string $distPath): bool
    {
        $hotPath = "${distPath}/hot";
        $devServerIsRunning = false;

        if (file_exists($hotPath)) {
            try {
                $devServerIsRunning = trim(file_get_contents($hotPath)) === 'development';
            } catch (Exception $e) {
                throw $e;
            }
        }

        return $devServerIsRunning;
    }
}
