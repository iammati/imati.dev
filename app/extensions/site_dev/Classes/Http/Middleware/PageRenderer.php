<?php

declare(strict_types=1);

namespace Site\Dev\Http\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Site\Dev\Service\ViteService;
use TYPO3\CMS\Core\Page\PageRenderer as T3PageRenderer;

class PageRenderer implements MiddlewareInterface
{
    protected T3PageRenderer $pageRenderer;
    protected ViteService $viteService;

    public function __construct(
        T3PageRenderer $t3PageRenderer,
        ViteService $viteService
    ) {
        $this->pageRenderer = $t3PageRenderer;
        $this->viteService = $viteService;
    }

    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface {
        $this->pageRenderer->addHeaderData(
            $this->viteService->render('head')
        );

        $this->pageRenderer->addFooterData(
            $this->viteService->render('body')
        );

        return $handler->handle($request);
    }
}
