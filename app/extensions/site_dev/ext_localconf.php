<?php

use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Utility\GeneralUtility;

(function () {
    if (TYPO3_MODE === 'BE') {
        /** @var PageRenderer */
        $pageRenderer = GeneralUtility::makeInstance(PageRenderer::class);
        $pageRenderer->addJsFile('EXT:site_dev/Resources/Public/JavaScript/backend.js');
    }
})();
