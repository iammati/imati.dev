<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Site-Dev',
    'category' => 'fe',
    'author' => 'Mati',
    'author_email' => 'mati_01@icloud.com',
    'state' => 'stable',
    'version' => '0.0.1',
    'constraints' => [
        'conflicts' => [],
        'suggests' => [],
        'depends' => [],
    ],
];
