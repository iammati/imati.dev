<?php

use Site\Dev\Http\Middleware;

return [
    'frontend' => [
        'site-dev/pagerenderer' => [
            'target' => Middleware\PageRenderer::class,

            'after' => [
                'typo3/cms-frontend/prepare-tsfe-rendering',
            ],

            'before' => [
                'typo3/cms-frontend/shortcut-and-mountpoint-redirect',
                'site-frontend/pagerenderer',
            ],
        ],
    ],
];
