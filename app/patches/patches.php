<?php

$patches = [
    'Custom-Overrides' => [
        '../typo3/sysext/core/Classes/Configuration/Event/AfterTcaCompilationEvent.php',
    ],
];

function applyPatches($patches)
{
    for ($i = 0; $i < 5; ++$i) {
        echo "\n";
    }

    echo "\n\n\n";

    echo "\n==============================================================================================================================\n\n";
    echo '| Applying patch on ' . gmdate('Y-m-d - H:i') . "\n\n\n";

    foreach ($patches as $sha => $files) {
        echo "| Preparing patch $sha...\n\n";

        foreach ($files as $file) {
            $destination = realpath("./../public/$file");
            $source = realpath("./app/patches/$file");

            $a = shell_exec("cp $source $destination 2>&1");

            if (null === $a) {
                echo "| Patch for file '$file' has been applied.\n";
            } else {
                echo "\n| Patch for file '$file' failed!\n| Log: ";
                var_dump($a);
                echo "\n";
            }
        }
    }

    echo "\n\n\n==============================================================================================================================\n\n\n";
}

applyPatches($patches);
